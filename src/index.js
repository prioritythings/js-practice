const wrapper = document.getElementById('page')
const header = document.getElementById('header')
const pageTitle = document.getElementById('page-title')
const pageContent = document.getElementById('page-content')
const footer = document.getElementById('footer')
const totalLessions = 3
const defaultLession = 3
let lessions = {}


const renderLession = async (lessionContent, lessionTitle, name) => {
  // Lazily load the requested page.
  const lession = await lessions[name]
  console.clear()
  pageContent.innerHTML = ''
  return lession.render(lessionContent, lessionTitle)
}
const buildLessionLink = (lession) => {
	let ele = document.createElement('button')
	ele.setAttribute('data-lession', lession)
	ele.id = lession
	ele.innerHTML = 'JS Practice - ' + lession
	ele.onclick = (e) => {
		e.preventDefault()
		//e.target.dataset.lession
		//e.target.getAttribute('data-lession')
		renderLession(pageContent, pageTitle, lession)
	}
	return ele	
}
const buildLessions = () => {
		
	let lessionLinks = document.createElement('div')
	for(let i = 1; i <= totalLessions; i++){
		let lession = "lession_" + i.toString().padStart(3,'0')
		lessions[lession] = import('./basic/' + lession + '.js')
		lessionLinks.appendChild(buildLessionLink(lession))
	}
	header.appendChild(lessionLinks)
}


window.onload = function(){
	pageTitle.innerHTML = 'You can learn everthing by clicking on lession button'
	buildLessions()
	document.getElementById("lession_" + defaultLession.toString().padStart(3,'0')).click()
}