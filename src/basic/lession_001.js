export const render = (page, pageTitle) =>{
	pageTitle.innerHTML = 'Lession 001'
	const PI = 3.141593;
	console.group('Lession 001')
	console.log('js practice index.js');
	console.log('PI=', PI);
	// start Arrow Functions
	console.group('Arrow Functions');
	const sum = function(x,y){
		return x + y;
	}
	console.warn('expression bodies')
	const sumExp = (x, y) => x + y
	const double = x => 2*x
	console.log('sum(2,3)=',sum(2,3))
	console.log('sum(2,3)=', sumExp(2,3))
	console.log('double(3)=', double(3))
	console.warn('statement bodies')
	const songs = [
		{id:1, name:"Quan trọng phải là thần thái", singer:"OnlyC", views: 1},
		{id:2, name:"Ngắm hoa lệ rơi", singer:"Châu Khải Phong", views: 3},
		{id:3, name:"Tâm sự tuổi 30", singer:"Trịnh Thăng Bình", views: 2},
		{id:4, name:"Bùa Yêu", singer:"Ái Phương", views: 4},
		{id:5, name:"Chia tay đòi quà", singer:"OnlyC", views: 5},
	];
	let singers = [];
	const songOf = singer => {
		let singer_songs = []
		songs.forEach(song => {
			if (song.singer === singer)
				singer_songs.push({id: song.id, name: song.name})
		})
		return singer_songs
	}
	console.info('songOf("OnlyC")=', songOf("OnlyC"))
	console.warn('Lexical this','More intuitive handling of current object context.')

	function Collection(name, songs, collectionId, rootEle){
		this.rootEle = rootEle
		this.collectionId = collectionId
		this.name = name
		this.songs = songs

		this.collectionName = (order) => {
			return this.name.replace('%n', this.songs.length) + " theo thứ tự " + (order==='desc' ? 'cao đến thấp' : 'thấp đến cao')
		}
		this.listAll = () => {
			this.collectionName()
			this.songs.forEach(song => console.info(song))				
		}
		this.render = (collectionName, songs) => {
			let collectionEle = document.createElement('div')
			collectionEle.id = this.collectionId
			let collectionNameEle = document.createElement('h3')
			collectionNameEle.innerHTML = collectionName
			collectionEle.appendChild(collectionNameEle)
			this.rootEle.appendChild(collectionEle)
			songs.forEach(song => {
				let songEle = this.renderSong(song)
				collectionEle.appendChild(songEle)
			})
		}
		this.renderSong = (song) => {
			let ele = document.createElement('div')
			ele.id = "song-" + song.id
			Object.keys(song).forEach(key => {
				ele.innerHTML += "<p><strong>" + key + ":</strong>" + song[key] + "</p>"
			})
			ele.innerHTML += '<hr/>'
			return ele
		}
	}

	Collection.prototype.listRank = function(order){
		this.render(
			this.collectionName(order),
			this.songs
			.sort( (a,b) => order==='desc' ? b.views - a.views : a.views - b.views )
			.map((song, index) => Object.assign({},song, {rank: order==='desc' ? index+1 : songs.length - index }))	
		)
	}


	const collection201811 = new Collection('Top %n bài hát hay nhất tháng 11/2018', songs, 'collection201811', page)
	collection201811.listAll()
	collection201811.listRank('desc')
	collection201811.listRank('asc')

	console.groupEnd();
	// end Arrow Functions
	console.groupEnd();
}