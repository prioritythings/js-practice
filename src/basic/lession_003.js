const socketIOClient = require('socket.io-client');
const sailsIOClient = require('sails.io.js');
const apiURL = 'http://localhost:1337';
let io = sailsIOClient(socketIOClient);
// io.sails.connect();
io.sails.autoConnect = true;
io.sails.url = apiURL
// io.sails.connect();


export const render = (page, pageTitle) => {
	console.clear()
 	pageTitle.innerHTML = 'Lession 003 - Sails Socket'
 	// page
 	let nickNameInput = document.createElement('input')
 	let breakLine = document.createElement('br')
 	let joinSocketBtn = document.createElement('button')
 	joinSocketBtn.innerHTML = 'Connect'
 	page.appendChild(nickNameInput)
 	page.appendChild(breakLine)
 	page.appendChild(joinSocketBtn)
 	page.appendChild(breakLine)

 	console.group('Lession 003')
 	// blueprint socket
 	io.socket.on('connect', function(msg){
 		console.log('connect')
 		console.log(msg)
 	})	

	// handle events
	joinSocketBtn.onclick = function(e){
		e.preventDefault();
		io.socket.post('/chat/connect', {
			nickname: nickNameInput.value
		}, function (resData, jwres){
			// resData === jwres.body
			console.group('/chat/connect')
			console.log(resData)
			console.log(jwres)
			console.groupEnd()
		})
	}

	io.socket.on('chat_connect', function (resData) {
 		console.group('sub:chat_connect')
 		console.log(resData)
 		console.groupEnd()
	}); 	
}
