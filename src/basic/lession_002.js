export const render = (page, pageTitle) => {
   pageTitle.innerHTML = 'Lession 002 - Basic Syntax'
   console.group('Lession 002')
   console.info('JavaScript Types are Dynamic: This means that the same variable can be used to hold different data types')
   const dataTypes = () => {
      const checkType = (value) => {
         console.log('value=', value, typeof value)
      }
      checkType()
      checkType(true)
      checkType(false)
      checkType("true")
      checkType("false")
      checkType('1')
      checkType(1)
      checkType(1.23456)
      checkType(1e5)
      checkType(1e-5)
      checkType([1,2,3,4])
      checkType({"0":"1","1":"2","2":"3"})
      checkType({firstName:"John", lastName:"Doe", age:50, eyeColor:"blue"})
      checkType(null)
      checkType(()=>{console.log('123')})
      checkType(new String("abc"))
      checkType(new Number(1))
      checkType(new Boolean(true))
   }
   dataTypes()
   console.info("Don't create strings,numbers,boolean as objects. It slows down execution speed")
   console.info('String Methods:')
   const stringMethods = () => {
      let text = 'ABC123ABCXYZABC'
      console.log('text.length', text.length)
      console.log("text.indexOf('ABC')",text.indexOf('ABC'))
      console.log("text.indexOf('A',1)",text.indexOf('A',1))
      console.log("text.indexOf(1,1)",text.indexOf(1,1))
      console.log("text.indexOf('1', 2)",text.indexOf('1', 2))
      console.log("text.lastIndexOf('ABC')",text.lastIndexOf('ABC'))
      console.log("text.indexOf('abc')",text.indexOf('abc'))
      console.log("text.search('a')", text.search('A'))
   }
   stringMethods()
   console.log((0.1 + 0.2), ':', (0.1 + 0.2).toString().length, '2/0 = Infinity', 2/0, '-2/0 = -Infinity', -2/0)
   page.innerHTML += '<h3>JavaScript Numbers are Always 64-bit Floating Point</h3>'
   console.groupEnd()   
}